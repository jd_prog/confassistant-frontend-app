import localeUkExtra from '@angular/common/locales/extra/uk';
import localeUk from '@angular/common/locales/uk';
import {registerLocaleData} from '@angular/common';
import localeEnExtra from '@angular/common/locales/extra/eu';
import localeEn from '@angular/common/locales/eu';

declare const require;

let language = 'en';

const languages = ['en', 'uk'];

languages.forEach(it => {
  navigator.languages.forEach( it1 => {
    if (it === it1) {
      language = it;
    }
  });
});


registerLocaleData(localeEn, 'en' , localeEnExtra);

const localesMap = [{'locale': 'en', 'localeMethod': localeEn, 'localeExtra': localeEnExtra}, {'locale': 'uk', 'localeMethod' : localeUk, 'localeExtra': localeUkExtra}];
localesMap.forEach( it => {
  if ( language === it.locale ) {
    registerLocaleData(it.localeMethod, it.locale, it.localeExtra);
  };
});



export const translations = require(`raw-loader!./locale/messages.${language}.xlf`);
