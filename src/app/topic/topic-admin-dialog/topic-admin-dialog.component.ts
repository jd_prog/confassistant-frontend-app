import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {ConfirmDialogWindowComponent, Event, EventsService, PopupsService, Speaker, SpeakersService, Topic, TopicService, TopicValidationService} from "qq-core-lib";
import {Router} from "@angular/router";
import * as moment from "moment";

@Component({
  selector: 'app-topic-admin-dialog',
  templateUrl: './topic-admin-dialog.component.html',
  styleUrls: ['./topic-admin-dialog.component.less']
})
export class TopicAdminDialogComponent implements OnInit {
  @ViewChild('name') title: ElementRef;
  @ViewChild('date') date: ElementRef;
  @ViewChild('timeStart') timeStart: ElementRef;
  @ViewChild('timeEnd') timeEnd: ElementRef;
  users: Array<Speaker> = [];
  filteredUsers: Array<Speaker> = [];
  event: Event;
  streamId: number;
  firstDayOfEvent: Date;
  lastDayOfEvent: Date;
  topics: Array<Topic> = [];
  topicDayAndMonth = new Date();
  confirmDialogWindows: MatDialogRef<ConfirmDialogWindowComponent>;
  selectedInputDate = [new Date(), new Date()];
  topic: Topic;
  constructor(private matDialogRef: MatDialogRef<TopicAdminDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private topicService: TopicService,
              private topicValidation: TopicValidationService,
              private eventService: EventsService,
              private router: Router,
              private speakerService: SpeakersService,
              private popupsService: PopupsService) {
  }

  ngOnInit() {
    this.speakerService.findAll().subscribe(usersList => {
      this.users = usersList;
      this.filteredUsers = usersList;
    });
    this.topic = Object.assign({}, this.data.topic);
    this.streamId = this.data.streamId;
    this.event = this.data.event;
    this.firstDayOfEvent = moment(this.data.event.dateStart).startOf('day').toDate();
    this.lastDayOfEvent = moment(this.data.event.dateEnd).endOf('day').toDate();
    this.topicDayAndMonth = new Date(this.data.topic.startDate);
    this.selectedInputDate[0] = new Date(this.data.topic.startDate);
    this.selectedInputDate[1] = new Date(this.data.topic.endDate);
    this.getTopics();
  }

  private getTopics() {
    this.topicService.getTopics(this.streamId).subscribe(topics => {
      this.topics = topics;
    });
  }

  handleFilter(value) {
    this.filteredUsers = this.users.filter((s) => s.displayName.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
  cancel() {
    this.matDialogRef.close();
  }
  save(topic: Topic) {
    if (this.validationAudit(topic)) {
      topic.startDate = moment(this.topicDayAndMonth).set({'h':this.selectedInputDate[0].getHours(), 'm':this.selectedInputDate[0].getMinutes()}).toDate();
      topic.endDate =moment(this.topicDayAndMonth).set({'h':this.selectedInputDate[1].getHours(), 'm':this.selectedInputDate[1].getMinutes()}).toDate();
      this.matDialogRef.close({topic: topic});
    }
  }

  showValidationWarning(inputElement: ElementRef, warning: string) {
    this.popupsService.showSnackBar(warning);
    inputElement.nativeElement.focus();
  }

  validationAudit(topic): boolean {
    if (this.topicValidation.isTitleNotEmpty(topic)) {
      this.showValidationWarning(this.title, 'Topic title cannot be empty');
      return;
    }
    if (this.topicValidation.isStatusValid(this.topicDayAndMonth)) {
      this.showValidationWarning(this.date, 'Topic date is invalid');
      return;
    }
    if (this.topicValidation.isDateOfTopicIncludeInEventRange(this.topicDayAndMonth, this.event)) {
      this.showValidationWarning(this.date, 'Topic date not included in range of holding event');
      return;
    }

    if (this.topicValidation.isStatusValid(this.selectedInputDate[0])){
      this.showValidationWarning(this.timeStart, 'Topic time start is invalid');
      return;
    }
    if (this.topicValidation.isStatusValid(this.selectedInputDate[1])) {
      this.showValidationWarning(this.timeEnd, 'Topic time end is invalid');
      return;
    }
    return true;
  }
}

