import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {AmazingTimePickerService} from 'amazing-time-picker';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {ConfirmDialogWindowComponent, Event, Stream, StreamEventService, StreamService, Topic, TopicEditService, TopicService} from 'qq-core-lib';

@Component({
  selector: 'app-topics-admin-view',
  templateUrl: './topics-admin-view.component.html',
  styleUrls: ['./topics-admin-view.component.less'],
  encapsulation: ViewEncapsulation.None,
  providers: [TopicEditService, TopicService, StreamService,
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}]
})
export class TopicsAdminViewComponent implements OnInit {
  event = new Event();
  stream = new Stream(0, '', '', []);
  toppics: Topic[] = [];
  topicId?: number;
  confirmDialogWindows: MatDialogRef<ConfirmDialogWindowComponent>;

  constructor(private topicService: TopicService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private dialog: MatDialog,
              private atp: AmazingTimePickerService,
              private topicEditService: TopicEditService,
              private streamService: StreamService,
              private streamEventService: StreamEventService) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.stream.id = +params['streamId'];
      this.event.id = +params['id'];
      this.topicService.getTopics(this.stream.id).subscribe(topicsReceived => {
        this.toppics = topicsReceived;
      });
    });

    this.streamService.getStreamById(this.stream.id.toString()).subscribe(stream => {
      this.stream = stream;
    });
    this.activatedRoute.params.subscribe(params => {
      if (params['topicId'] !== undefined || params['topicId'] !== null) {
        this.topicId = +params['topicId'];
      }
    });

    this.topicEditService.getEditedTopic().subscribe(editedTopic => {
      this.toppics.filter(topic => editedTopic.id === topic.id ? editedTopic : topic);
    });
  }
}
