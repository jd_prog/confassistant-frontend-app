import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../material/material.module';
import {TopicsAdminViewComponent} from './topics-admin-view/topics-admin-view.component';
import {SecurityModule} from '../security/security.module';
import {TopicAdminPanelComponent} from './topic-admin-panel/topic-admin-panel.component';
import {FormsModule} from '@angular/forms';
import {NgPipesModule} from 'ng-pipes';

import {NgArrayPipesModule} from 'ngx-pipes';
import {RouterModule} from '@angular/router';
import {PipesModule, QqTopicModule} from 'qq-core-lib';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {InputsModule} from '@progress/kendo-angular-inputs';
import {MatDialogModule} from "@angular/material";
import {TopicAdminDialogComponent} from './topic-admin-dialog/topic-admin-dialog.component';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';

@NgModule({
  imports: [
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    CommonModule,
    MaterialModule,
    RouterModule,
    SecurityModule,
    FormsModule,
    NgPipesModule,
    NgArrayPipesModule,
    PipesModule,
    QqTopicModule,
    DropDownsModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    InputsModule,
    MatDialogModule
  ],
  declarations: [
    TopicsAdminViewComponent,
    TopicAdminPanelComponent,
    TopicAdminDialogComponent
  ],
  exports: [
    TopicsAdminViewComponent,
    TopicAdminPanelComponent
  ],
  entryComponents: [
    TopicAdminDialogComponent
  ]
})
export class TopicModule {
}
