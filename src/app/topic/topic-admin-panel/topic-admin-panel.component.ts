import {Component, OnInit} from '@angular/core';

import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatDialogRef} from '@angular/material';
import {ConfirmDialogWindowComponent, Event, EventsService, SpeakersService, Topic, TopicService} from 'qq-core-lib';
import * as moment from 'moment';
import {TopicAdminDialogComponent} from "../topic-admin-dialog/topic-admin-dialog.component";
import {filter, flatMap, map} from "rxjs/operators";
import {Observable} from "rxjs/Observable";
import * as firebase from "firebase";
import Timestamp = firebase.firestore.Timestamp;

@Component({
  selector: 'app-topic-admin-panel',
  templateUrl: './topic-admin-panel.component.html',
  styleUrls: ['./topic-admin-panel.component.less']
})
export class TopicAdminPanelComponent implements OnInit {
  event: Event;
  streamId: number;
  eventsDays: Set<Date>;
  topics: Array<Topic> = [];
  confirmDialogWindows: MatDialogRef<ConfirmDialogWindowComponent>;
  saveDataOfTopicDialog: MatDialogRef<TopicAdminDialogComponent>;


  constructor(private topicService: TopicService,
              private eventService: EventsService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              public dialog: MatDialog,
              private speakerService: SpeakersService
  ) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
        this.streamId = +params['streamId'];
        this.eventService.findById(params['id']).subscribe(event => {
          this.event = event;
          this.initEventDays(event);
        });
        this.getTopics();
      }
    );
  }

  private getTopics() {
    this.topicService.getTopics(this.streamId).subscribe(topics => {
      this.topics = topics;
    });
  }

  addTopic(dateOfTopic: Date) {
    const dayTopics = this.topics.filter(it => moment(dateOfTopic).isSame(it.endDate, 'day'))
      .sort((a:Topic, b:Topic) =>
      {
       return moment(a.startDate).unix() - moment(b.endDate).unix()});
    const lastTopic = dayTopics.length >= 0 ? dayTopics[dayTopics.length - 1] : null;
    let newTopicStart;
    if (lastTopic != null) {
      const lastTopicEnd = lastTopic.endDate;
      newTopicStart = moment(lastTopicEnd).add(15, 'minute');
    } else {

      newTopicStart = moment(dateOfTopic).set({ h: 8, m: 0, s: 0, milliseconds:0});
    }
    const newTopicEnd = moment(newTopicStart).add(1, 'hour');
    const topicToCreate = new Topic();
    topicToCreate.startDate = newTopicStart.toDate();
    topicToCreate.endDate = newTopicEnd.toDate();
    this.openTopic(topicToCreate).subscribe(topic => this.topics.push(topic));
  }

  openTopic(topic: Topic): Observable<Topic> {
    this.saveDataOfTopicDialog = this.dialog.open(TopicAdminDialogComponent, {
      data: {
        event: this.event,
        streamId: this.streamId,
        topic: topic
      }
    });

    return this.saveDataOfTopicDialog.afterClosed().pipe(
      filter(dialogResult => !!dialogResult),
      flatMap((dialogResult: { topic: Topic }) =>
        dialogResult.topic.id ? this.topicService.
        update(this.streamId, dialogResult.topic) : this.topicService.create(this.streamId, dialogResult.topic)),
      map(updatedTopic => Object.assign(topic, updatedTopic))
    )
  }

  deleteTopic(topic: Topic) {
    this.confirmDialogWindows = this.dialog.open(ConfirmDialogWindowComponent, {
      data: {
        text: 'Are you sure want to remove',
        entity: topic,
        positiveAnswer: 'Yes',
        negativeAnswer: 'No'
      }
    });
    this.confirmDialogWindows.afterClosed().subscribe(confirm => {
      if (confirm) {
        let indexOfTopic = this.topics.indexOf(topic);
        this.topics.splice(indexOfTopic, 1);
        this.topicService.delete(topic.id);
      }
    });
  }

  initEventDays(event: Event) {
    this.eventsDays = new Set<Date>();
    const firstEventDay = moment(event.dateStart);
    const lastEventDay = moment(event.dateEnd);
    let day = firstEventDay;
    while (day <= lastEventDay) {
      this.eventsDays.add(day.startOf('day').toDate());
      day = day.add(1, 'day');
    }
  }
}
