import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {Attachment, AttachmentService, ConfirmDialogWindowComponent, Event, EventsService, EventUpdateService} from 'qq-core-lib';
import {GalleryComponent, GalleryItem, ImageItem} from '@ngx-gallery/core';

@Component({
  selector: 'app-upload-photo',
  templateUrl: './upload-photo.component.html',
  styleUrls: ['./upload-photo.component.less']
})
export class UploadPhotoComponent implements OnInit {
  @ViewChild('gallery') gallery: GalleryComponent;
  image: File;
  url: Attachment;
  imagesAttachment: Array<Attachment>;
  images: GalleryItem[];
  idOfOpenedImage: number;
  confirmDialogWindows: MatDialogRef<ConfirmDialogWindowComponent>;

  constructor(private attachmentService: AttachmentService,
              @Inject(MAT_DIALOG_DATA) public data: { event: Event, openPhoto: boolean },
              public dialogRef: MatDialogRef<UploadPhotoComponent>,
              public dialog: MatDialog,
              private eventUpdateService: EventUpdateService,
              private eventsService: EventsService
  ) {
  }

  ngOnInit(): void {
    this.setCollectionOfImages(this.data.event);
  }

  changeIdOfOpenedImage(event) {
    const id = event.currIndex;
    if (id + 1 > this.images.length) {
      this.gallery.set(0);
      this.idOfOpenedImage = this.images[0].data.id;
    } else {
      this.idOfOpenedImage = this.images[id].data.id;
    }
  }

  clear() {
    this.image = undefined;
    this.data.openPhoto = false;
  }

  add() {
    this.data.openPhoto = false;
  }

  processing(file: File) {
    this.image = file;
    const reader = new FileReader();
    reader.onload = (e) => {
      this.url = new Attachment();
      this.url.downloadUri = e.target.result;
    };
    reader.readAsDataURL(file);
    this.data.openPhoto = true;
  }

  setCollectionOfImages(event: Event) {
    this.imagesAttachment = [];
    this.images = [];
    if (this.data.openPhoto && event.attachments && event.attachments.length !== 0) {
      this.imagesAttachment = event.attachments.filter(e =>
        this.validType(e.type)
      );
      this.reformatAttchment();
      this.idOfOpenedImage = this.imagesAttachment[0].id;
    } else {
      this.data.openPhoto = false;
    }
  }

  validType(type: string): boolean {
    return type === 'image/png' || type === 'image/jpeg' || type === 'image/jpg';
  }

  reformatAttchment() {
    this.images = this.imagesAttachment.map(img =>
      new ImageItem({src: img.downloadUri, thumb: img.downloadUri, id: img.id}));
  }

  close(): void {
    this.dialogRef.close();
  }

  delete() {
    this.confirmDialogWindows = this.dialog.open(ConfirmDialogWindowComponent, {
      data: {
        text: 'Are you sure want to remove a photo',
        entity: '',
        positiveAnswer: 'Yes',
        negativeAnswer: 'No'
      }
    });
    this.confirmDialogWindows.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.deleteImg();
      }
    });
  }

  deleteFromEvent() {
    this.eventsService.deleteAttachment(this.data.event.id, this.idOfOpenedImage).then(event => this.setCollectionOfImages(event));
  }

  deleteImg() {
    this.deleteFromEvent();
    this.setDisplayedImage();
  }

  private setDisplayedImage() {
    const indexOfDeletingImage = this.images.findIndex(img => this.idOfOpenedImage === img.data.id);
    if (indexOfDeletingImage === this.images.length - 1) {
      this.gallery.set(indexOfDeletingImage + 1);
    } else {
      this.gallery.set(indexOfDeletingImage);
    }
  }
  upload() {
    this.attachmentService.saveAttachment(this.image).subscribe(e => {
      this.eventsService.addAttachmentToEvent(this.data.event.id, e.id).subscribe(event => {
        this.eventUpdateService.notifyEventChange(event);
        this.data.event = event;
        this.image = undefined;
        this.data.openPhoto = true;
        this.setCollectionOfImages(this.data.event);
      });
    });
  }
}
