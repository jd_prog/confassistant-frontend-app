import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  ConfirmDialogWindowComponent,
  Event,
  EventsService,
  EventUpdateService,
  User,
  UserDetailsService
} from 'qq-core-lib';
import {flatMap, map, tap} from 'rxjs/operators';
import {MatDialog, MatDialogRef} from '@angular/material';
import 'rxjs/add/operator/mergeMap';
import {ShareEventComponent} from '../share-event/share-event.component';

@Component({
  selector: 'app-events-admin-create',
  templateUrl: './events-admin-create.component.html',
  styleUrls: ['./events-admin-create.component.less']
})
export class EventsAdminCreateComponent implements OnInit {

  confirmDialogWindows: MatDialogRef<ConfirmDialogWindowComponent>;
  event: Event;
  constructor(private activatedRoute: ActivatedRoute,
              private eventsService: EventsService,
              private router: Router,
              private eventUpdateService: EventUpdateService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    if (!this.router.url.endsWith('new')) {
      this.activatedRoute.params.pipe(
        map(params => params['id']),
        flatMap(id => this.eventsService.findById(id)),
        tap(event => {
        this.event = event;
      }))
        .subscribe();
    }

    this.eventUpdateService.subject.subscribe(event => {
      this.event = event;
    });
  }
  publish() {
    this.confirmDialogWindows = this.dialog.open(ConfirmDialogWindowComponent,
      {
        data: {
          text: 'Are you sure want to publish',
          entity: this.event, positiveAnswer: 'Yes', negativeAnswer: 'No'
        }
      });
    this.confirmDialogWindows.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.publishEvent();
      }
    });
  }

  publishEvent() {
    this.eventsService
        .publishEvent(this.event.id).subscribe(receivedEvent => {
          this.event = receivedEvent;
          this.eventUpdateService.notifyEventChange(this.event);
      });
  }

  toEventList() {
    this.router.navigate(['/events']);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ShareEventComponent , {
      width: '850px',
      height: '605',
      data: {
        event: this.event
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
