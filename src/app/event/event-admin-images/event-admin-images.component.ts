import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {
  Event,
  Attachment,
  AttachmentService,
  ApiUrlProvider,
  EventUpdateService,
  PopupsService
} from 'qq-core-lib';
import {MatDialog} from '@angular/material';
import {UploadPhotoComponent} from '../upload-photo/upload-photo.component';
import {Directive, EventEmitter, HostListener, Output} from '@angular/core';

@Component({
  selector: 'app-event-admin-images',
  templateUrl: './event-admin-images.component.html',
  styleUrls: ['./event-admin-images.component.less']
})
export class EventAdminImagesComponent implements OnInit {
  @Input('event')
  event: Event;
  image: File;
  url: Attachment;
  images: Array<Attachment> = [];

  file: File;

  constructor(private attachmentService: AttachmentService,
              private urlProvider: ApiUrlProvider,
              private eventUpdateService: EventUpdateService,
              public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.uri(this.event);
    this.eventUpdateService.subject.subscribe(event => {
      this.event = event;
      this.uri(this.event);
    });
  }


  uri(event: Event) {
    if (Array.isArray(event.attachments) && event.attachments.length !== 0) {
      this.images = event.attachments.filter(file =>
        this.validType(file.type)
      );
      this.url = this.images[0];
    } else {
      this.url = undefined;
    }
  }

  validType(type: string): boolean {
    return type === 'image/png' || type === 'image/jpeg' || type === 'image/jpg';
  }

  openDialog(openPhoto: boolean): void {
    const dialogRef = this.dialog.open(UploadPhotoComponent, {
      width: '850px',
      height: '605',
      data: {
        event: this.event,
        openPhoto: openPhoto
      }
    });
    dialogRef.afterClosed();
  }

}
