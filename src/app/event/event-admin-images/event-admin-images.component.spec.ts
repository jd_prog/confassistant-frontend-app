import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EventAdminImagesComponent} from './event-admin-images.component';

describe('EventAdminImagesComponent', () => {
  let component: EventAdminImagesComponent;
  let fixture: ComponentFixture<EventAdminImagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EventAdminImagesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventAdminImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
