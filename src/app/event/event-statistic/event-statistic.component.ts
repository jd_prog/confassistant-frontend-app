import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {
  Event,
  EventActualVisitorsService,
  EventNavigationSubjectService,
  EventPotentialVisitorsService,
  EventsService,
  SpeakersService, User
} from 'qq-core-lib';
import {ActivatedRoute} from '@angular/router';
import {MatPaginator, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-event-statistic',
  templateUrl: './event-statistic.component.html',
  styleUrls: ['./event-statistic.component.less']
})
export class EventStatisticComponent implements OnInit , OnDestroy {

  private event: Event;
  private subscription: Subscription;
  actualVisitors = new Array<User>();
  potentialVisitors = new Array<User>();
  userStatistic = new Array<object>();
  actualUser = new Array<object>();
  displayedColumns: string[] = ['username',  'potential', 'actual'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: MatTableDataSource<any>;

  constructor(private eventService: EventsService,
              private activatedRoute: ActivatedRoute,
              private speakerService: SpeakersService,
              private eventPotentialVisitorsService: EventPotentialVisitorsService,
              private eventActualVisitorsService: EventActualVisitorsService,
              private eventSaveNotifier: EventNavigationSubjectService) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.eventService.findById(params['id']).subscribe(event => {
        this.event = event;
        this.speakerService.findAllEventUsers(this.event.id).subscribe( next => {
          this.userStatistic = next;
          this.eventActualVisitorsService.getAllActualVisitors(this.event.id).subscribe( actualVisitors => {
            this.actualVisitors = actualVisitors;
            this.actualVisitors.forEach(it2 => {
              this.userStatistic.forEach(it => {
                if (it['username'] === it2.username) {
                  it['actual'] = 'Yes';
                  it['potential'] = 'No';
                }
              });
            });
          });
        this.eventPotentialVisitorsService.getAllPotentialVisitors(this.event.id).subscribe( potentialVisitors => {
          this.potentialVisitors = potentialVisitors;
          this.potentialVisitors.forEach( it2 => {
            this.userStatistic.forEach(it => {
              if (it['username'] === it2.username) {
                it['potential'] = 'Yes';
                if (it['actual'] !== 'Yes'){
                  it['actual'] = 'No';
                }
              }
            });
          });
        });
        
          this.dataSource = new MatTableDataSource<any>(this.userStatistic);
          this.dataSource.paginator = this.paginator;
        });
      });
    });
    this.subscription = this.eventSaveNotifier.subscribe((routeEnabler) => {
      routeEnabler.redirectEnabler.next(
        {
          allowRedirect: true,
          eventId: this.event.id.toString()
        });
    });
  }

  ngOnDestroy(): void {
    this.actualUser = [];
    this.subscription.unsubscribe();
  }
}

