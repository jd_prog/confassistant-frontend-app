/**
 * Created by vlad on 20.04.18.
 */
import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog, MatDialogRef} from '@angular/material';
import {
  ConfirmDialogWindowComponent,
  ParticipantsService,
  Event,
  EventsService, EventPotentialVisitorsService, User, Attachment
} from 'qq-core-lib';

@Component({
  selector: 'app-admin-events-list-item',
  templateUrl: './admin.events-list-item.component.html',
  styleUrls: ['./admin.events-list-item.component.less']
})
export class AdminEventsListItemComponent implements OnInit {
ulls


  @Input('event')
  event: Event;
  potentialVisitorsCount: number;
  potentialVisiotrs = new Array<User>();
  confirmDialogWindows: MatDialogRef<ConfirmDialogWindowComponent>;
  url: Attachment;
  private images: Array<string> = [];

  constructor(private router: Router,
              private eventService: EventsService,
              private eventPotentialVisitorsService: EventPotentialVisitorsService,
              public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.uri(this.event);
    this.eventPotentialVisitorsService.getAllPotentialVisitors(this.event.id).subscribe(potentialVisiotrs => {
      this.potentialVisiotrs = potentialVisiotrs;
    });
  }

  removeEvent() {
    this.confirmDialogWindows = this.dialog.open(ConfirmDialogWindowComponent,
      {
        data: {
          text: 'Are you sure want to remove',
          entity: this.event, positiveAnswer: 'Yes', negativeAnswer: 'No'
        }
      });
    this.confirmDialogWindows.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.delete(this.event);
      }
    });
  }

  uri(event: Event) {
    if (Array.isArray(event.attachments) && event.attachments.length !== 0) {
      this.url = event.attachments.filter(file =>
        this.validType(file.type)
      )[0];
    } else {
      this.url = undefined;
    }
  }

  validType(type: string): boolean {
    return type === 'image/png' || type === 'image/jpeg' || type === 'image/jpg';
  }

  delete(event) {
    this.eventService.deleteEvent(event);
  }
}
