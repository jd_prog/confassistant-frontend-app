import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PopupsService} from 'qq-core-lib';
import {UploadPhotoComponent} from '../upload-photo/upload-photo.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-add-photo',
  templateUrl: './add-photo.component.html',
  styleUrls: ['./add-photo.component.less']
})
export class AddPhotoComponent implements OnInit {
  @Output() image = new EventEmitter<File>();
  @Input('event')
  event: Event;
  dropzoneActive = false;
  dropzoneRed = false;

  constructor(private popupsService: PopupsService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  dropzoneState($event: boolean) {
    setTimeout(() => {
      this.dropzoneActive = $event;
    }, 200);
  }

  handleDrop(fileList: FileList) {
    if (fileList.length !== 0 && this.validType(fileList.item(0).type)) {
      this.checkingFile(fileList.item(0));
    } else {
      this.dropzoneRed = true;
      setTimeout(() => {
        this.dropzoneRed = false;
      }, 3000);
      this.popupsService.showSnackBar('Invalid type of photo');
    }
  }

  validType(type: string): boolean {
    return type === 'image/png' || type === 'image/jpeg' || type === 'image/jpg';
  }

  checkingFile(file: File) {
    if (file) {
      this.image.emit(file);
    }
  }
}
