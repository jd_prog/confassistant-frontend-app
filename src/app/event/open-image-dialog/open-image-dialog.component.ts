import {Component, Input, OnInit} from '@angular/core';
import {UploadPhotoComponent} from '../upload-photo/upload-photo.component';
import {MatDialog} from '@angular/material';
import {
  Event
} from 'qq-core-lib';

@Component({
  selector: 'app-open-image-dialog',
  templateUrl: './open-image-dialog.component.html',
  styleUrls: ['./open-image-dialog.component.less']
})
export class OpenImageDialogComponent implements OnInit {
  @Input('event')
  event: Event;

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(UploadPhotoComponent, {
      width: '850px',
      height: '605',
      data: {
        event: this.event,
        openPhoto: false
      }
    });
  }
}
