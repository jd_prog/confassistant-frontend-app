import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenImageDialogComponent } from './open-image-dialog.component';

describe('OpenImageDialogComponent', () => {
  let component: OpenImageDialogComponent;
  let fixture: ComponentFixture<OpenImageDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenImageDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenImageDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
