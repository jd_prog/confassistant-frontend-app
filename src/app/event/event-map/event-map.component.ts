/// <reference types="@types/googlemaps" />
import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  NgZone,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {MapsAPILoader} from '@agm/core';
import {FormControl} from '@angular/forms';
import {Location, LocationChangeService, GoogleGeocoderService, EventMapService} from 'qq-core-lib';



@Component({
  selector: 'app-event-map',
  templateUrl: './event-map.component.html',
  styleUrls: ['./event-map.component.less']
})
export class EventMapComponent implements AfterViewInit, OnInit {

  @Input() eventLocation: Location;
  @ViewChild('search')
  public searchElementRef: ElementRef;
  public searchControl: FormControl;
  place: google.maps.places.PlaceResult;
  constructor(private changeLocationService: LocationChangeService,
              private mapsApiLoader: MapsAPILoader,
              private ngZone: NgZone,
              private eventMapService: EventMapService) { }


  ngAfterViewInit() {
    this.mapsApiLoader.load().then(() => {
      const  autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          this.place = autocomplete.getPlace();
          this.eventLocation.formattedAddress = autocomplete.getPlace().formatted_address;
          if ( this.place.geometry === undefined || this.place.geometry === null) {
            return;
          }
          this.eventLocation.latitude = this.place.geometry.location.lat();
          this.eventLocation.longitude = this.place.geometry.location.lng();
          this.changeLocationService.changeLocation(this.eventLocation);
          const mapEvent = { coords: {lat: this.place.geometry.location.lat(), lng: this.place.geometry.location.lng()}};
          this.changeMarker(mapEvent, this.eventLocation);
        });
      });
    }).catch( erro => {
    });
  }

  changeMarker(event, eventLocation: Location) {
    this.eventMapService.changeMarker(event, eventLocation);
  }


  ngOnInit(): void {
    this.eventMapService.getLocation(this.eventLocation);
    this.searchControl = new FormControl();
  }
}
