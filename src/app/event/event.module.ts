import {NgModule} from '@angular/core';
import {MaterialModule} from '../material/material.module';
import {CommonModule} from '@angular/common';
import {EventAdminComponent} from './events-admin-view/event.admin.component';
import {AdminEventsListItemComponent} from './events-admin-list-item/admin.events-list-item.component';
import {EventsAdminDetailsComponent} from './events-admin-details/events-admin-details.component';
import {FormsModule} from '@angular/forms';
import {AdminCoreModule} from '../admin/admin-core/admin-core.module';
import {SecurityModule} from '../security/security.module';
import {EventsAdminCreateComponent} from './events-admin-create/events-admin-create.component';
import {NgArrayPipesModule} from 'ngx-pipes';
import {AgmCoreModule} from '@agm/core';
import {environment} from '../../environments/environment';
import {EventMapComponent} from './event-map/event-map.component';
import {ComponentsModule, PipesModule, QQEventModule} from 'qq-core-lib';
import {RouterModule} from '@angular/router';
import {ShareEventComponent} from './share-event/share-event.component';
import {EventNavigationComponent} from './event-navigation/event-navigation.component';
import {EventStatisticComponent} from './event-statistic/event-statistic.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {UploadPhotoComponent} from './upload-photo/upload-photo.component';
import {EventAdminImagesComponent} from './event-admin-images/event-admin-images.component';
import {QrCodeComponent} from './qr-code/qr-code.component';
import {FileDropDirective} from './file-drop.directive/file-drop.directive';
import {AddPhotoComponent} from './add-photo/add-photo.component';
import {OpenImageDialogComponent} from './open-image-dialog/open-image-dialog.component';
import {GalleryModule} from '@ngx-gallery/core';
import {LightboxModule} from '@ngx-gallery/lightbox';
import {GallerizeModule} from '@ngx-gallery/gallerize';
import { NgxImageGalleryModule } from 'ngx-image-gallery';

@NgModule({
  imports: [
    GalleryModule,
    LightboxModule,
    GallerizeModule,
    NgxImageGalleryModule,
    CommonModule,
    MaterialModule,
    FormsModule,
    AdminCoreModule,
    SecurityModule,
    NgArrayPipesModule,
    MatPaginatorModule,
    MatTableModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleMapsKey,
      language: 'en',
      libraries: ['geometry', 'places']
    }),
    PipesModule,
    RouterModule,
    QQEventModule,
    MatChipsModule,
    ComponentsModule
  ],
  declarations: [
    EventAdminComponent,
    AdminEventsListItemComponent,
    EventsAdminDetailsComponent,
    EventsAdminCreateComponent,
    EventMapComponent,
    ShareEventComponent,
    EventNavigationComponent,
    EventStatisticComponent,
    UploadPhotoComponent,
    EventAdminImagesComponent,
    QrCodeComponent,
    FileDropDirective,
    AddPhotoComponent,
    OpenImageDialogComponent
  ],

  providers: [],

  exports: [
    EventAdminComponent,
    AdminEventsListItemComponent,
    EventsAdminDetailsComponent,
    EventsAdminCreateComponent,
    ShareEventComponent
  ],
  entryComponents: [
    ShareEventComponent,
    UploadPhotoComponent
  ]
})
export class EventModule {
}
