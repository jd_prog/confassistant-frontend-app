import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {
  AdminDetailsServiceService,
  ApiUrlProvider,
  Event,
  EventEditorService,
  EventNavigationSubjectService,
  EventPotentialVisitorsService,
  EventsService,
  EventUpdateService,
  Location,
  LocationChangeService,
  PopupsService,
  StreamService,
  Tag,
  Attachment,
  AttachmentService,
  TagService,
  User
} from 'qq-core-lib';
import {Observable} from 'rxjs/Observable';
import {flatMap, map, tap} from 'rxjs/operators';
import {Subscription} from 'rxjs/Subscription';
import {FormControl, Validators} from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material';
import {GalleryItem, ImageItem} from '@ngx-gallery/core';

@Component({
  selector: 'app-events-admin-details',
  templateUrl: './events-admin-details.component.html',
  styleUrls: ['./events-admin-details.component.less']
})
export class EventsAdminDetailsComponent implements OnInit, OnDestroy {
  @ViewChild('title') title: ElementRef;
  @ViewChild('dateStartIn') dateStartIn: ElementRef;
  @ViewChild('dateEndIn') dateEndIn: ElementRef;
  @ViewChild('participants') participants: ElementRef;
  @ViewChild('inputImg') inputImg: ElementRef;

  dataHasBeenChanged = false;

  event: Event = new Event();
  dateStart = new Date();
  dateEnd = new Date();
  mapChangeSubscription: Subscription;
  potentialVisitors = new Array<User>();
  selectable = true;
  removable = true;
  addOnBlur = true;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  participantsLimit = new FormControl(this.event.participantsLimit, [
    Validators.pattern('[0-9][0-9]{0,4}$'),
  ]);
  private navigateSubscription: Subscription;


  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private eventsService: EventsService,
    private streamService: StreamService,
    private locationChangeSerive: LocationChangeService,
    private eventsDetailsAdminService: AdminDetailsServiceService,
    private eventUpdateService: EventUpdateService,
    private eventEditorService: EventEditorService,
    private popupsService: PopupsService,
    private eventPotentialVisitors: EventPotentialVisitorsService,
    private eventSaveNotifier: EventNavigationSubjectService,
    private tagService: TagService,
  ) {
  }

  ngOnInit() {
    if (this.router.url.endsWith('new')) {
      this.event.location = new Location();
      navigator.geolocation.getCurrentPosition(position => {
        this.event.location.latitude = position.coords.latitude;
        this.event.location.longitude = position.coords.longitude;
      });

    } else {
      this.activatedRoute.params.pipe(
        map(params => params['id']),
        flatMap(eventId => this.loadEvent(eventId)),
        tap(event => {
          this.event = event;

          this.refreshEventPotentialVisitors();
          this.eventsDetailsAdminService.assignLocation(this.event);
          this.resizeDateRangeToMidnights(this.event.dateStart, this.event.dateEnd);
        }))

        .subscribe();
    }

    this.eventUpdateService.subject.subscribe(event => {
      this.event = event;
    });

    this.mapChangeSubscription = this.locationChangeSerive.subject.subscribe(next => {
      this.saveEvent();
    });

    this.eventEditorService.requestLocationChanges(location => {
      this.event.location = location;
      this.dataHasBeenChanged = true;
    });

    this.navigateSubscription = this.eventSaveNotifier.subscribe((routeEnabler) => {
      this.observableSaveEvent().subscribe((isValidEvent) =>
        routeEnabler.redirectEnabler.next(
          {
            allowRedirect: isValidEvent,
            eventId: this.event.id.toString()
          })
      );
    });
  }

  checkImageIs(): boolean {
    return !!this.event && this.event.attachments.filter(file =>
      this.validType(file.type)).length !== 0;
  }

  validType(type: string): boolean {
    return type === 'image/png' || type === 'image/jpeg' || type === 'image/jpg';
  }

  ngOnDestroy(): void {
    this.eventEditorService.unsubscribeAll();
    this.navigateSubscription.unsubscribe();
    this.mapChangeSubscription.unsubscribe();
  }

  private loadEvent(eventId: number): Observable<Event> {
    return this.eventsService.findById(eventId)
      .pipe(tap(eventReceived => {
        this.event = eventReceived;
        if (this.event.location == null) {
          this.event.location = new Location();
          this.event.location.longitude = 0;
          this.event.location.latitude = 0;
        }
        this.dateStart = new Date(this.event.dateStart);
        this.dateEnd = new Date(this.event.dateEnd);
      }));
  }

  dateUpdate() {
    this.resizeDateRangeToMidnights(this.dateStart, this.dateEnd);
    if (this.dateStart > this.dateEnd) {
      this.dateEnd = this.dateStart;
    }
    const topics = this.eventEditorService.parseEventTopics(this.event);
    console.log();
    if (topics.length !== 0) {
      if (this.eventEditorService.isEventDatesPassedToTopicsDates(this.dateStart, this.dateEnd, topics)) {
        this.popupsService.showSnackBar('Please check out you topics scheduler \n before changing event range');
        this.dateEnd = new Date(this.event.dateEnd);
        this.dateStart = new Date(this.event.dateStart);
      } else {
        this.saveEvent();
      }
    } else {
      this.saveEvent();
    }
  }

  private resizeDateRangeToMidnights(dateStart: Date, dateEnd: Date) {
    this.dateStart = moment(dateStart).startOf('day').toDate();
    this.dateEnd = moment(dateEnd).endOf('day').toDate();
  }

  private validationAudit(): boolean {
    if (!this.eventEditorService.isEventValid(this.event)) {
      this.showValidationWarning(this.title, 'Please, enter event name');
      return;
    }
    if (this.eventEditorService.isValidParticipantLimit(this.event)) {
      this.showValidationWarning(this.participants, 'Please, enter valid participants limit');
      return;
    }
    if (this.eventEditorService.isValidDate(this.dateStartIn.nativeElement.value)) {
      this.showValidationWarning(this.dateStartIn, 'Please, enter valid date start limit');
      return;
    }
    if (this.eventEditorService.isValidDate(this.dateEndIn.nativeElement.value)) {
      this.showValidationWarning(this.dateEndIn, 'Please, enter valid date end limit');
      return;
    }
    return true;
  }

  saveEvent() {
    this.eventEditorService.setDatesRangeToEvent(this.event, this.dateStart, this.dateEnd);
    if (this.validationAudit()) {
      const dataSaved =
        (this.event.id > 0 ? this.eventEditorService.updateEvent(this.event) : this.eventEditorService
          .createEvent(this.event));
      dataSaved.subscribe(event => {
        this.event = event;
        this.refreshEventPotentialVisitors();
        this.notifyEventChanged();
      });
    }
  }

  private observableSaveEvent() {
    this.eventEditorService.setDatesRangeToEvent(this.event, this.dateStart, this.dateEnd);
    if (!this.validationAudit()) {
      return Observable.create(false);
    }
    const dataSaved =
      (this.event.id > 0 ? this.eventEditorService.updateEvent(this.event) : this.eventEditorService.createEvent(this.event));
    return dataSaved.pipe(
      tap(event => {
        this.notifyEventChanged();
      }),
      map(() => true));

  }

  private notifyEventChanged() {
    const shareEvent = new Event();
    Object.assign(shareEvent, this.event);
    this.eventUpdateService.notifyEventChange(shareEvent);
  }

  private showValidationWarning(inputElement: ElementRef, warning: string) {
    this.popupsService.showSnackBar(warning);
    inputElement.nativeElement.focus();
  }

  refreshEventPotentialVisitors() {
    this.eventPotentialVisitors.getAllPotentialVisitors(this.event.id).subscribe(potentialVisitors => {
      this.potentialVisitors = potentialVisitors;
    });
  }


  add(matChipInputEvent: MatChipInputEvent, event: Event): void {
    const input = matChipInputEvent.input;
    const value = matChipInputEvent.value;
    if ((value || '').trim()) {
      const tag = new Tag();
      tag.name = value.trim();
      this.tagService.create(tag).subscribe(tagToCreate => {
        this.tagService.addTagToEvent(`${event.id}`, `${tagToCreate.id}`).subscribe(tagToAdd => {
          event.tags.push(tagToAdd);
        });
      });
    }
    if (input) {
      input.value = '';
    }
  }

  remove(event: Event, tag: Tag): void {
    this.tagService.removeFromEvent(`${event.id}`, `${tag.id}`).subscribe(next => {
      const index = event.tags.indexOf(tag);
      if (index >= 0) {
        event.tags.splice(index, 1);
      }
    });
  }

}
