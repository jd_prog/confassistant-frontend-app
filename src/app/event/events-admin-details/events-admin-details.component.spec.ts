import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {EventsAdminDetailsComponent} from './events-admin-details.component';
import {
  Tag,
  TagService,
  Event,
  AdminDetailsServiceService,
  EventsService,
  EventPotentialVisitorsService,
} from 'qq-core-lib';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {BrowserTestingModule} from '@angular/platform-browser/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {of} from 'rxjs';
import {MaterialModule} from '../../material/material.module';
import {MatChipInputEvent, MatChipsModule} from '@angular/material/chips';
import {NgArrayPipesModule} from 'ngx-pipes';
import {Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing';


const mockRoutes: Routes = [
  { path: '', component: EventsAdminDetailsComponent }
];

describe('EventsAdminDetailsComponent', () => {
  let component: EventsAdminDetailsComponent;
  let fixture: ComponentFixture<EventsAdminDetailsComponent>;

  let tagService;
  let eventService;
  let eventPotentialVisitors;
  let adminDetailsServiceService;
  beforeEach( async(() => {
    tagService = jasmine.createSpyObj('TagService', ['create', 'addTagToEvent', 'removeFromEvent']);
    eventService = jasmine.createSpyObj( 'EventsService', ['findById']);
    eventPotentialVisitors = jasmine.createSpyObj(' EventPotentialVisitorsService', ['getAllPotentialVisitors']);
    adminDetailsServiceService = jasmine.createSpyObj('AdminDetailsServiceService', ['assignLocation']);


    TestBed.configureTestingModule({
      imports: [
        BrowserTestingModule,
        RouterTestingModule.withRoutes(mockRoutes),
        BrowserAnimationsModule,
        MaterialModule,
        MatChipsModule,
        NgArrayPipesModule,
        FormsModule,
        HttpClientTestingModule
      ],
      declarations: [
        EventsAdminDetailsComponent
      ],
      providers: [
        {provide: TagService, useValue: tagService},
        {provide: EventsService, useValue: eventService},
        {provide: EventPotentialVisitorsService, useValue: eventPotentialVisitors},
        {provide: AdminDetailsServiceService, useValue: adminDetailsServiceService},
        ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents().then( () => {
        const tag = new Tag();
        const event = new Event();
        const users = [ {username: 'name'}, {username: 'name2'}];
        tagService.create.and.returnValues( of(tag) );
        eventService.findById.and.returnValues( of(event));
        eventPotentialVisitors.getAllPotentialVisitors.and.returnValues( of(users));
      fixture = TestBed.createComponent(EventsAdminDetailsComponent);
      component = fixture.componentInstance;
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should add tag to event tags', () => {
    const tag = new Tag();
    const event = new Event();
    event.tags = [];
    tagService.create.and.returnValues( of(tag) );
    tagService.addTagToEvent.and.returnValues( of(tag));
    const input = jasmine.createSpyObj('HTMLInputElement', ['']);
    input['value'] = 'tag';
    const value = 'tag';
    tag.name = 'tag';
    let matChipInputElement: MatChipInputEvent;
    matChipInputElement = {input: input, value: value};
    component.add(matChipInputElement, event);
    expect(event.tags).toContain(tag);
    if (input) {
      expect(input.value).toEqual('');
    }
  });


  it('should remove tag from event tags',  () => {
    const tag = new Tag();
    const event = new Event();
    event.tags = [];
    event.tags.push(tag);
    tagService.removeFromEvent.and.returnValues( of({}));
    component.remove(event, tag);
    expect(event.tags).not.toContain(tag);
  });
  it('should not contain tag in events tags',  () => {
    const tag = new Tag();
    const event = new Event();
    event.tags = [];
    tagService.removeFromEvent.and.returnValues( of({}));
    component.remove(event, tag);
    expect(event.tags.indexOf(tag)).toEqual(-1);
  });
});
