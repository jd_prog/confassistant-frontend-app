import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {
  ApiUrlProvider,
  Event,
  EventsService,
  Speaker,
  SpeakersService,
  EventUpdateService,
  EventSharedUsersService
} from 'qq-core-lib';


@Component({
  selector: 'app-share-event',
  templateUrl: './share-event.component.html',
  styleUrls: ['./share-event.component.less']
})
export class ShareEventComponent implements OnInit {
  users: Array<Speaker> = [];
  filteredUsers: Array<Speaker> = [];
  searching = '';
  sharedUsers = [];


  constructor(public dialogRef: MatDialogRef<ShareEventComponent>,
              public eventService: EventsService,
              public eventSharedUsers: EventSharedUsersService,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              private speakerService: SpeakersService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.speakerService.findAll().subscribe(usersList => {
      this.users = usersList;
      this.filteredUsers = usersList;
    });
    this.eventSharedUsers.getAllSharedUsers(this.data.event.id).subscribe(sharedUsers => {
      this.sharedUsers = sharedUsers;
    });
  }

  usersList() {
    return this.filteredUsers.filter(s => !this.sharedUsers.find((user => user.username === s.username)))
      .filter(s => s.username !== this.data.event.creator.username)
      .filter(s => s.displayName.toLowerCase().indexOf(this.searching.toLowerCase()) !== -1);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  add() {
    this.eventSharedUsers.addAllSharedUsers(this.sharedUsers, this.data.event.id).subscribe(response => {
      this.dialogRef.close();
    });
  }

  deleteSelectedUser(user) {
    this.sharedUsers.splice(this.sharedUsers.indexOf(user), 1);
  }

  addUsersToContributors(user) {
    this.sharedUsers.push(user);
  }

}

export interface DialogData {
  username: string;
  event: Event;
}
