import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {DialogData, ShareEventComponent} from './share-event.component';
import {
  Event,
  EventSharedUsersService,
  EventsService,
  EventUpdateService,
  Speaker,
  SpeakersService,
  StatusEvent,
  User
} from "qq-core-lib";
import {FormsModule} from "@angular/forms";
import {MaterialModule} from "../../material/material.module";
import {MAT_DIALOG_DATA, MatDialog, MatDialogModule, MatDialogRef} from "@angular/material";
import {Observable} from "rxjs";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FlexLayoutModule} from "@angular/flex-layout";

class MockSpeakerService {
  findAll() {
  }
}

class MockEventSharedUsersService {
  getAllSharedUsers() {
  }

  addAllSharedUsers() {

  }
}

class MockDialogRef {
  close() {
  }
}

describe('ShareEventComponent', () => {
  let component: ShareEventComponent;
  let fixture: ComponentFixture<ShareEventComponent>;
  let speakerService: SpeakersService;
  let eventSharedUsersService: EventSharedUsersService;
  let dialogRef: MatDialogRef<ShareEventComponent>;
  let fakeUsersList: Array<Speaker>;
  let fakeSharedUsersList: Array<User>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, MaterialModule, MatDialogModule, BrowserAnimationsModule, FlexLayoutModule ],
      declarations: [ShareEventComponent],
      providers: [
        {provide: SpeakersService, useClass: MockSpeakerService},
        {provide: EventSharedUsersService, useClass: MockEventSharedUsersService},
        {provide: EventsService, useValue: {}},
        {provide: EventUpdateService, useValue: {}},
        {provide: MAT_DIALOG_DATA, useValue: data},
        {provide: MatDialogRef, useClass: MockDialogRef},
        {provide: MatDialog, useValue: {}}
      ]
    })
      .compileComponents();
  }));
  const fakeCreator: User = {
    username: 'maxnelipa',
  };
  const fakeEvent: Event = {
    id: 7,
    title: '',
    description: '',
    location: null,
    dateStart: null,
    dateEnd: null,
    streams: null,
    status: StatusEvent.PUBLISH,
    participantsLimit: 0,
    creator: fakeCreator,
    tags: []
  };
  const fakeUser: User = {
    username: 'George',
  };
  const data: DialogData = {
    event: fakeEvent,
    username: fakeUser.username
  };
  beforeEach(() => {
    fakeUsersList = [
      {
        username: 'maxnelipa',
        email: 'maxnelipa@gmail.com',
        displayName: 'maxnelipa',
        image: ''
      },
      {
        username: 'John',
        email: 'john@gmail.com',
        displayName: 'jonny',
        image: ''
      },
      {
        username: 'George',
        email: 'georgio@gmail.com',
        displayName: 'georgio',
        image: ''
      },
      {
        username: 'Michel',
        email: 'michel@gmail.com',
        displayName: 'Michel',
        image: ''
      },
      {
        username: 'Vlad',
        email: 'vlad@gmail.com',
        displayName: 'vlad',
        image: ''
      },
      {
        username: 'Andrew',
        email: 'andrew@gmail.com',
        displayName: 'andrew',
        image: ''
      },
      {
        username: 'Pol',
        email: 'pol@gmail.com',
        displayName: 'Pol',
        image: ''
      }
    ];
    fakeSharedUsersList = [{
      username: 'John',
    },
      {
        username: 'George'
      }
    ];
    speakerService = TestBed.get(SpeakersService);
    eventSharedUsersService = TestBed.get(EventSharedUsersService);
    spyOn(speakerService, 'findAll').and.returnValues(Observable.of(fakeUsersList));
    spyOn(eventSharedUsersService, 'getAllSharedUsers').and.returnValues(Observable.of(fakeSharedUsersList));
    fixture = TestBed.createComponent(ShareEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should event list and filtered list return after start ngOnInit', () => {
    expect(component.users).toEqual(fakeUsersList);
    expect(component.filteredUsers).toEqual(fakeUsersList);
  });
  it('should shared event list return after start ngOnInit', () => {
    expect(component.sharedUsers).toEqual(fakeSharedUsersList);
  });
  it('should filter user list without search', () => {
    const fakeFilteredUserList: Array<Speaker> = [
      {
        username: 'Michel',
        email: 'michel@gmail.com',
        displayName: 'Michel',
        image: ''
      },
      {
        username: 'Vlad',
        email: 'vlad@gmail.com',
        displayName: 'vlad',
        image: ''
      },
      {
        username: 'Andrew',
        email: 'andrew@gmail.com',
        displayName: 'andrew',
        image: ''
      },
      {
        username: 'Pol',
        email: 'pol@gmail.com',
        displayName: 'Pol',
        image: ''
      }
    ];
    const filteredUserList = component.usersList();
    expect(filteredUserList).toEqual(fakeFilteredUserList);
  });
  it('should filtered user list with search', () => {
    const fakeFilteredUserListWithSerch = [
      {
        username: 'Michel',
        email: 'michel@gmail.com',
        displayName: 'Michel',
        image: ''
      }
    ];
    component.searching = 'el';
    const filteredUserList = component.usersList();

    expect(filteredUserList).toEqual(fakeFilteredUserListWithSerch);
  });

  it('should add user to shared list', () => {
    const fakeUserForAdd: User = {
      username: 'Pol'
    };
    const fakeSharedUsersListAfterAdd: Array<User> = [
      {
        username: 'John'
      },
      {
        username: 'George'
      },
      {
        username: 'Pol'
      }
    ];
    component.addUsersToContributors(fakeUserForAdd);
    expect(component.sharedUsers).toEqual(fakeSharedUsersListAfterAdd);
  });
  it('should delete user from shared list', () => {
    const fakeSharedUsersListAfterDelete: Array<User> = [
      {
        username: 'George'
      }
    ];
    const fakeUserForDelete = component.sharedUsers.find(user =>
      user.username.toString() === 'John');
    component.deleteSelectedUser(fakeUserForDelete);
    expect(component.sharedUsers).toEqual(fakeSharedUsersListAfterDelete);
  });
  it('should add users to editors', () => {
    dialogRef = TestBed.get(MatDialogRef);
    spyOn(eventSharedUsersService, 'addAllSharedUsers').and.returnValues(Observable.of(fakeSharedUsersList));
    spyOn(dialogRef, 'close');
    component.add();
    expect(component.eventSharedUsers.addAllSharedUsers).toHaveBeenCalledWith(fakeSharedUsersList, data.event.id);
    expect(component.dialogRef.close).toHaveBeenCalled();
  });
  it('should closed dialog window', () => {
    dialogRef = TestBed.get(MatDialogRef);
    spyOn(dialogRef, 'close');
    component.onNoClick();
    expect(component.dialogRef.close).toHaveBeenCalled();
  });
});
