import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Router} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {EventAdminComponent} from "./event.admin.component";
import {FormsModule} from "@angular/forms";
import {MaterialModule} from "../../material/material.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import {
  Event,
  EventSharedUsersService,
  EventsService,
  EventStatusService,
  FilterByPredicatePipe,
  NotPublished,
  PublishedEvents,
  SortByDatePipe,
  StatusEvent,
  User,
  UserDetailsService
} from "qq-core-lib";
import {Observable} from "rxjs";
import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, PipeTransform} from '@angular/core';

class MockRouter {
  navigate(){};
}
class MockEventsService {
  findAllCreatedByCurrentUser() {
  }
  getEventsContainer(){}
}

class MockEventSharedUsersService {
  getAllEventShared() {
  }
}
class MockEventStatusService {
  notPassedEvents() {}

  lasting() {}

  passed() {}

  afterNextMonth() {}

  onNextMonth() {}

  onThisMonth() {}
}

class MockUserDetailsService {
  getCurrentUserUsername(){}
}
class MockPublishedEvents implements PipeTransform {
    transform(){}
  }

 class MockFilterByPredicatePipe implements PipeTransform {
  transform(events: Array<any>, args: any) { return events};
}

 class MockSortByDatePipe implements PipeTransform {
  transform(){}
}

class MockNotPublished implements PipeTransform {
  transform(){}
}


describe('EventAdminComponent', () => {
  let component: EventAdminComponent;
  let fixture: ComponentFixture<EventAdminComponent>;
  let eventsService: EventsService;
  let eventSharedUsers: EventSharedUsersService;
  let eventStatus: EventStatusService;
  let userDetailsService: UserDetailsService;
  let router: Router;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        MaterialModule,
        BrowserAnimationsModule,
        FlexLayoutModule,

      ],
      declarations: [
        EventAdminComponent,
        PublishedEvents,
        FilterByPredicatePipe,
        SortByDatePipe,
        NotPublished,

      ],
      providers: [
        {provide: Router, useClass: MockRouter},
        {provide: EventsService, useClass: MockEventsService},
        {provide: EventSharedUsersService, useClass: MockEventSharedUsersService},
        {provide: EventStatusService, useClass: MockEventStatusService},
        {provide: UserDetailsService, useClass: MockUserDetailsService},
        {provide: PublishedEvents, useClass: MockPublishedEvents},
        {provide: FilterByPredicatePipe, useClass: MockFilterByPredicatePipe},
        {provide: SortByDatePipe, useClass: MockSortByDatePipe},
        {provide: NotPublished, useClass: MockNotPublished},
        {provide: SortByDatePipe, useClass: MockSortByDatePipe},
        ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));
  const fakeCreator: User = {
    username: 'maxnelipa',
  };
  const fakeCreatorOfSharedEvent: User = {
    username: 'fillipmoris',
  };
  const fakeEventsList: Array<Event> = [
    {
      id: 3,
      title: 'Java',
      description: '',
      location: null,
      dateStart: null,
      dateEnd: null,
      streams: null,
      status: StatusEvent.PUBLISH,
      participantsLimit: 0,
      creator: fakeCreator,
      tags: []
    },
    {
      id: 7,
      title: 'TypeScript',
      description: '',
      location: null,
      dateStart: null,
      dateEnd: null,
      streams: null,
      status: StatusEvent.PUBLISH,
      participantsLimit: 0,
      creator: fakeCreator,
      tags: []
    },
    {
      id: 9,
      title: 'JavaScript',
      description: '',
      location: null,
      dateStart: null,
      dateEnd: null,
      streams: null,
      status: StatusEvent.PUBLISH,
      participantsLimit: 0,
      creator: fakeCreator,
      tags: []
    },
    {
      id: 4,
      title: 'Kafka',
      description: '',
      location: null,
      dateStart: null,
      dateEnd: null,
      streams: null,
      status: StatusEvent.PUBLISH,
      participantsLimit: 0,
      creator: fakeCreator,
      tags: []
    },
    {
      id: 1,
      title: 'SQL',
      description: '',
      location: null,
      dateStart: null,
      dateEnd: null,
      streams: null,
      status: StatusEvent.PUBLISH,
      participantsLimit: 0,
      creator: fakeCreator,
      tags: []
    },
    {
      id: 90,
      title: 'Kotlin',
      description: '',
      location: null,
      dateStart: null,
      dateEnd: null,
      streams: null,
      status: StatusEvent.PUBLISH,
      participantsLimit: 0,
      creator: fakeCreator,
      tags: []
    }
  ];
  const fakeSharedEvents: Array<Event> = [{
      id: 23,
      title: 'Java',
      description: '',
      location: null,
      dateStart: null,
      dateEnd: null,
      streams: null,
      status: StatusEvent.PUBLISH,
      participantsLimit: 0,
      creator: fakeCreatorOfSharedEvent,
      tags: []
    }];
  beforeEach(() => {
    eventsService = TestBed.get(EventsService);
    userDetailsService = TestBed.get(UserDetailsService);
      eventSharedUsers = TestBed.get(EventSharedUsersService);
      eventStatus = TestBed.get(EventStatusService);
    spyOn(eventsService, 'findAllCreatedByCurrentUser').and.returnValue(Observable.of(fakeEventsList));
     spyOn(eventsService, 'getEventsContainer').and.returnValue(Observable.of(fakeEventsList));
     spyOn(userDetailsService, 'getCurrentUserUsername').and.returnValues(Observable.of(fakeCreator.username));
      spyOn(eventSharedUsers, 'getAllEventShared').and.returnValues(Observable.of(fakeSharedEvents));

      spyOn(eventStatus, 'lasting');
      spyOn(eventStatus, 'passed');
      spyOn(eventStatus, 'afterNextMonth');
      spyOn(eventStatus, 'onThisMonth');
      spyOn(eventStatus, 'onNextMonth');
      spyOn(eventStatus, 'notPassedEvents');
    fixture = TestBed.createComponent(EventAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should start ngOnInit', () => {
    expect(eventsService.findAllCreatedByCurrentUser).toHaveBeenCalled();
    expect(eventsService.getEventsContainer).toHaveBeenCalled();
    expect(component.events).toEqual(fakeEventsList);
    expect(component.username).toEqual(fakeCreator.username);
    expect(component.sharedEvents).toEqual(fakeSharedEvents);
  });
  it('should filters start', () => {
    expect(eventStatus.afterNextMonth).toHaveBeenCalled();
    expect(eventStatus.notPassedEvents).toHaveBeenCalled();
    expect(eventStatus.lasting).toHaveBeenCalled();
    expect(eventStatus.onThisMonth).toHaveBeenCalled();
    expect(eventStatus.onNextMonth).toHaveBeenCalled();
    expect(eventStatus.passed).toHaveBeenCalled();
  });
  it('should  navigate', () => {
    router = TestBed.get(Router);
    spyOn(router, 'navigate');
    component.createEvent();
    expect(router.navigate).toHaveBeenCalledWith(['events', 'new']);
  });

  });
