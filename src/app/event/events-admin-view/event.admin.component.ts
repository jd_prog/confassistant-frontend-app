/**
 * Created by vlad on 20.04.18.
 */
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {
  Event,
  EventSharedUsersService,
  EventsService,
  EventStatusService,
  StatusEvent, User,
  UserDetailsService
} from 'qq-core-lib';

@Component({
  selector: 'app-event-admin',
  templateUrl: 'event.admin.component.html',
  styleUrls: ['event.admin.component.less']
})
export class EventAdminComponent implements OnInit {
  events: Array<Event>;
  sharedEvents: Array<Event> = [];
  username: string;
  constructor(private router: Router,
              private userDetailsService: UserDetailsService,
              private eventsService: EventsService,
              private eventSharedUsers: EventSharedUsersService,
              private eventStatus: EventStatusService) {
  }

  ngOnInit() {
    this.eventsService.findAllCreatedByCurrentUser().subscribe();
    this.eventsService.getEventsContainer().subscribe(events => {
      this.events = events;
      this.userDetailsService.getCurrentUserUsername().subscribe(username => {
        this.username = username;
        this.eventSharedUsers.getAllEventShared(this.username)
          .subscribe(sharedEvents => {
              this.sharedEvents = sharedEvents;
            }
          );
      });
    })
  }
  // this has to be arrow functions to bind this

  lasting = (event: Event) => this.eventStatus.lasting(event);

  passed = (event: Event) => this.eventStatus.passed(event);

  afterNextMonth = (event: Event) => this.eventStatus.afterNextMonth(event);

  onNextMonth = (event: Event) => this.eventStatus.onNextMonth(event);

  onThisMonth = (event: Event) => this.eventStatus.onThisMonth(event);

  notPassedEvents = (event: Event) => this.eventStatus.notPassedEvents(event);

  createEvent() {
    this.router.navigate(['events', 'new']);
  }
}

