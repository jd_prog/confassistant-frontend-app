import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {ApiUrlProvider, Event} from 'qq-core-lib';

@Component({
  selector: 'app-qr-code',
  templateUrl: './qr-code.component.html',
  styleUrls: ['./qr-code.component.less']
})
export class QrCodeComponent implements OnInit, OnChanges {
  @Input('event')
  event: Event;
  downloadLink: string;
  qrImage: string;

  constructor(
    private urlProvider: ApiUrlProvider
  ) {
  }

  ngOnChanges() {
    this.qrGenerate(this.event);

  }

  ngOnInit() {
  }

  qrGenerate(event: Event) {
    this.downloadLink = this.urlProvider.getUrl() + `api/events/${event.id}/qrcode`;
    this.qrImage = this.downloadLink;
  }

}
