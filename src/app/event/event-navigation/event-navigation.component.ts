import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject, Subscription} from "rxjs";
import {filter} from "rxjs/operators";
import {ActivatedRoute, NavigationStart, Router} from '@angular/router';
import {
  EventNavigationSubjectService,
  EventsService,
  Event,
  EventUpdateService,
  EventPotentialVisitorsService,
  EventActualVisitorsService, User
} from 'qq-core-lib';
import {RouterEvent} from "@angular/router/src/events";

@Component({
  selector: 'app-event-navigation',
  templateUrl: './event-navigation.component.html',
  styleUrls: ['./event-navigation.component.less']
})
export class EventNavigationComponent implements OnInit, OnDestroy {
  currentUrl = '';
  routeSubscription: Subscription;
  potentialVisitors = new Array<User>();
  actualVisitors = new Array<User>();
  event = new Event();
  constructor(
    private navigateNotificator: EventNavigationSubjectService,
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private eventService: EventsService,
    private eventPotentialVisitorsService: EventPotentialVisitorsService,
    private eventActualVisitorsService: EventActualVisitorsService,
    private eventUpdateService: EventUpdateService) { }
  detailsAllowRedirect = new Subject<{allowRedirect: boolean, eventId: string}>();
  streamsAllowRedirect = new Subject<{allowRedirect: boolean, eventId: string}>();
  statisticAllowRedirect = new Subject<{allowRedirect: boolean, eventId: string}>();



  redirectToStreams(){
    this.navigateNotificator.next({name: 'streams', redirectEnabler: this.streamsAllowRedirect});
  }

  redirectToDetails(){
    this.navigateNotificator.next({name: 'details', redirectEnabler: this.detailsAllowRedirect});
  }

  redirectToStatistic() {
    this.navigateNotificator.next({name: 'statistic', redirectEnabler: this.statisticAllowRedirect});
  }

  ngOnInit() {
    this.currentUrl = this.router.url;
    if (!this.router.url.endsWith('new')) {
       this.activatedRouter.params.subscribe( params => {
         this.eventService.findById(params['id']).subscribe(event => {
           this.event = event;
           this.eventPotentialVisitorsService.getAllPotentialVisitors(this.event.id).subscribe(potentialVisitors => {
             this.potentialVisitors = potentialVisitors;
           });
           this.eventActualVisitorsService.getAllActualVisitors(this.event.id).subscribe( actualVisitors => {
             this.actualVisitors = actualVisitors;
           });
         });
       });
    }

    this.routeSubscription = this.router.events.pipe(filter((e: RouterEvent) => e instanceof NavigationStart)).subscribe((e) => {
      this.currentUrl = e.url;
    });

    this.detailsAllowRedirect.asObservable().pipe(
      filter(permission => permission.allowRedirect)
    ).subscribe(permission => {
      this.router.navigate(['events', permission.eventId, 'details'], {replaceUrl: true})
    });

    this.streamsAllowRedirect.asObservable().pipe(
      filter(permission => permission.allowRedirect)
    ).subscribe(permission => {
      this.router.navigate(['events', permission.eventId, 'streams'], {replaceUrl: true})
    });

    this.statisticAllowRedirect.asObservable().pipe(
      filter(permission => permission.allowRedirect)
    ).subscribe(permission => {
      this.router.navigate(['events', permission.eventId, 'statistic'], {replaceUrl: true})
    });
  }

  ngOnDestroy(): void {
    this.detailsAllowRedirect.unsubscribe();
    this.streamsAllowRedirect.unsubscribe();
    this.statisticAllowRedirect.unsubscribe();
    this.routeSubscription.unsubscribe();
  }

}
