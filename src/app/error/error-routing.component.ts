import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ErrorNotFoundPageComponent} from './error-not-found-page/error-not-found-page.component';
import {ErrorForbiddenPageComponent} from './error-forbidden-page/error-forbidden-page.component';


const routes: Routes = [
  {path: 'forbidden', component: ErrorForbiddenPageComponent},
  {path: '**', redirectTo: '/error', pathMatch: 'full'},
  {path: 'error', component: ErrorNotFoundPageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorRoutingComponent {
}

