import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginAdminComponent} from './login-admin/login-admin.component';
import {LoginPageGuard} from 'qq-core-lib';

const routes: Routes = [
  {
    path: 'login', component: LoginAdminComponent, canActivate: [LoginPageGuard], data: {
      destinationRoute: ['events']
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecurityRoutingModule {
}
