import {Component, OnDestroy, OnInit} from '@angular/core';
import {Oauth2Service} from '../security-core/services/oauth2.service';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {LinkedInService} from 'angular-linkedin-sdk';
import {AuthService, FacebookLoginProvider, GoogleLoginProvider} from 'angular5-social-login';
import {LinkedinLoginProvider} from 'angular5-social-auth';
import {environment} from '../../../environments/environment.test';

@Component({
  selector: 'app-login-admin',
  templateUrl: './login-admin.component.html',
  styleUrls: ['./login-admin.component.less']
})
export class LoginAdminComponent implements OnInit {


  constructor(private socialAuthService: AuthService,
              private oauthService: Oauth2Service,
              private linkedInService: LinkedInService,
              private router: Router) {
  }


  ngOnInit() {
  }

  registerAdmin(sociaclNetworkName: string) {
    if ( environment.production) {
      if (sociaclNetworkName === 'facebook') {
        this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID)
          .then(socialUser => {
              this.login(socialUser.id, socialUser.name, socialUser.image);
            }
          );
      } else if (sociaclNetworkName === 'google') {
        this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID)
          .then(socialUser => {
              this.login(socialUser.id, socialUser.name, socialUser.image);
            }
          );
      } else if (sociaclNetworkName === 'linkedin') {
        this.socialAuthService.signIn(LinkedinLoginProvider.PROVIDER_ID)
          .then(socialUser => {
              this.login(socialUser.id, socialUser.name, socialUser.image);
            }
          );
      }
    } else {
        this.login('id', 'user', '');
    }
  }
  private  login(username: string, password: string, image: string) {

    this.oauthService.authorizeAdmin(username, password, image)
      .subscribe(success => {
        if (success) {
          this.router.navigate(['events']);
        }
      });

  }
}
