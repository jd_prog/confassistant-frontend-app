/**
 * Created by yana on 30.04.18.
 */
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {ApiUrlProvider} from 'qq-core-lib';
import {Observable} from 'rxjs/Observable';
import 'rxjs-compat/add/operator/concatMap';
import {UserDetailsService} from 'qq-core-lib';


@Injectable()
export class Oauth2Service {

  basicAuthToken = 'Basic Y29uZmFzc2lzdGFudDpzZWNyZXQ=';

  constructor(private http: HttpClient,
              private urlProvider: ApiUrlProvider,
              private userDetailsService: UserDetailsService) {
  }

  authorize(userId: string,
            displayName: string, image: string): Observable<boolean> {

    return this.registerUser(userId, displayName, image)
      .concatMap((user) => this.authenticate( user['username'], user['password']));
  }

  authorizeAdmin(userId: string,
                 displayName: string, image: string): Observable<boolean> {
    return this.registerAdmin(userId, displayName, image)
      .concatMap((admin) => this.authenticate(admin['username'], admin['password']));
  }

  authenticate(username: string,
        password: string): Observable<boolean>  {

    return this.requestToken(username, password)
      .concatMap((authData) => this.parseAndSaveAuthData(authData))
      .concatMap(() => this.requestUserInfo())
      .concatMap(user => this.userDetailsService.setCurrentUserDisplayName(user['displayName'])
        .concatMap(() => this.userDetailsService.setCurrentUserUsername(user['username']))
        .concatMap(() => this.userDetailsService.setCurrentUserRole(user['role'])));
  }

  logout(): Observable<boolean> {
    return this.userDetailsService.clearCurrentUserDetails();
  }

  requestToken(username: string, password: string): Observable<any> {

    return this.http.post(
      `${this.urlProvider.getUrl()}oauth/token`,
      `grant_type=password&username=${username}&password=${password}`,
      {headers: {'Content-Type': 'application/x-www-form-urlencoded'}, withCredentials: true});
  }

  private requestUserInfo():  Observable<any> {
    return this.http.get(`${this.urlProvider.getUrl()}users/me`);
  }


  refreshToken(): Observable<boolean> {

    return this.userDetailsService.getCurrentUserRefreshToken()
      .concatMap(refreshToken => {
        return this.http.post(
          `${this.urlProvider.getUrl()}oauth/token`,
          `grant_type=refresh_token&refresh_token=${refreshToken}`,
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}, withCredentials: true});
      })
      .concatMap(authData => {
        if (authData) {
          return this.parseAndSaveAuthData(authData);
        }
      });
  }

  private registerUser(username: string, displayName: string, image: string): Observable<any> {
    return this.http.post(
      `${this.urlProvider.getUrl()}users/register`,
      {
        'username': username,
        'displayName': displayName,
        'image': image
      });
  }
  private registerAdmin(username: string, displayName: string, image: string): Observable<any> {
    return this.http.post(
      `${this.urlProvider.getUrl()}users/register/admin`,
      {
        'username': username,
        'displayName': displayName,
        'image': image

      }
    );
  }

  parseAndSaveAuthData(authData: Object): Observable<boolean> {
    const accessToken = authData['access_token'];
    const refreshToken = authData['refresh_token'];

    return this.userDetailsService.setCurrentUserAccessToken(accessToken)
      .concatMap(() => this.userDetailsService.setCurrentUserRefreshToken(refreshToken));

  }

  public isAuthenticated(): Observable<boolean> {

    return this.userDetailsService.getCurrentUserAccessToken()
      .map(accessToken => accessToken !== null);
  }
}
