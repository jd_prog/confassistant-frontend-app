import {NgModule} from '@angular/core';
import {Oauth2Service} from './services/oauth2.service';
import {
  AuthServiceConfig,
  FacebookLoginProvider,
  GoogleLoginProvider,
  SocialLoginModule
} from 'angular5-social-login';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthInterceptor} from './interceptors/auth.interceptor';
import {AdminAuthGuard} from './guards/admin-auth.guard';
import {LocalStorageModule} from '@ngx-pwa/local-storage';
import {environment} from '../../../environments/environment';
import { LinkedInSdkModule } from 'angular-linkedin-sdk';
import {LinkedinLoginProvider} from 'angular5-social-auth';


export function getAuthServiceConfigs() {
  return new AuthServiceConfig(
    [{
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider('571362676400579')
      },
      {id: GoogleLoginProvider.PROVIDER_ID,
       provider: new GoogleLoginProvider(environment.googleCredentials.web.client_id)
      },
      { id: LinkedinLoginProvider.PROVIDER_ID,
        provider: new LinkedinLoginProvider(environment.linkedId.client_id)
       }
       ]);
}

@NgModule({
  imports: [
    SocialLoginModule,
    LocalStorageModule,
    LinkedInSdkModule
  ],
  providers: [
    { provide: 'apiKey', useValue: environment.linkedId.client_id },
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    Oauth2Service,
    AdminAuthGuard
  ]
})
export class SecurityCoreModule {
}
