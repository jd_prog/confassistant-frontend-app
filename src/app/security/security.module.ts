import {NgModule} from '@angular/core';
import {MaterialModule} from '../material/material.module';
import {SecurityCoreModule} from './security-core/security-core.module';
import {SecurityRoutingModule} from './security-routing.module';
import {AdminAuthGuard} from './security-core/guards/admin-auth.guard';
import {LoginAdminComponent} from './login-admin/login-admin.component';
import {FormsModule} from '@angular/forms';
import {environment} from '../../environments/environment';

@NgModule({
  imports: [
    SecurityCoreModule,
    MaterialModule,
    SecurityRoutingModule,
    FormsModule
  ],
  declarations: [
    LoginAdminComponent
  ],
  providers: [
    AdminAuthGuard,
    { provide: 'apiKey', useValue: environment.linkedId.client_id }
  ],
  exports: [
    LoginAdminComponent
  ]
})
export class SecurityModule {}
