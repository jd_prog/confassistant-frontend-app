import {AfterViewInit, Component, ElementRef, Input, OnInit, Renderer, ViewChild} from '@angular/core';
import {Stream, StreamService, StreamCreateService, StreamEventService, PopupsService} from 'qq-core-lib';
import {ActivatedRoute, Router} from '@angular/router';
import {MatButton} from "@angular/material";
import {FormControl, Validators} from "@angular/forms";


@Component({
  selector: 'app-admin-add-stream',
  templateUrl: './stream-addmin-add-stream.component.html',
  styleUrls: ['./stream-addmin-add-stream.component.less']
})
export class StreamAddminAddStreamComponent implements OnInit {
  streamAdd = '';
  enterKey = 13;
  @ViewChild('input') inputEl:ElementRef;
  constructor(private streamService: StreamService,
              private activatedRouter: ActivatedRoute,
              private streamCreateService: StreamCreateService,
              private streamEventService: StreamEventService,
              private route: Router,
              private popupsService: PopupsService) {
  }
  ngOnInit(): void {
    this.inputEl.nativeElement.focus();

  }
  addStream() {
    if (this.streamAdd === '') {
      this.popupsService.showSnackBar('Name of stream cannot be empty')
    } else {
      this.activatedRouter.params.subscribe(params => {
        this.streamService.add(params['id'], new Stream(0, ' ', this.streamAdd, []))
          .subscribe(receivedStream => {
            this.streamCreateService.AddStream(receivedStream);
          });
      });
    }
  }
  cancle() {
    this.streamCreateService.cancleAdd();
  }

}
