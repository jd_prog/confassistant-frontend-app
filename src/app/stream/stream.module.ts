import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../material/material.module';
import {StreamsAdminDetailsComponent} from './streams-admin-details/streams-admin-details.component';
import {TopicModule} from '../topic/topic.module';
import {FormsModule} from '@angular/forms';
import {SecurityModule} from '../security/security.module';
import {StreamAdminCreateComponent} from './stream-admin-create/stream-admin-create.component';
import {RouterModule} from '@angular/router';
import {StreamAddminAddStreamComponent} from './stream-addmin-add-stream/stream-addmin-add-stream.component';
import {InputsModule} from '@progress/kendo-angular-inputs';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    TopicModule,
    FormsModule,
    SecurityModule,
    RouterModule,
    InputsModule

  ],
  declarations: [
    StreamsAdminDetailsComponent,
    StreamAdminCreateComponent,
    StreamAddminAddStreamComponent
  ],
  exports: [
    StreamsAdminDetailsComponent,
    StreamAdminCreateComponent,
    StreamAddminAddStreamComponent
  ]
})
export class StreamModule { }
