import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PopupsService, Stream, StreamEventService, StreamService} from 'qq-core-lib';
import {MatButton} from '@angular/material';

@Component({
  selector: 'app-stream-admin-create',
  templateUrl: './stream-admin-create.component.html',
  styleUrls: ['./stream-admin-create.component.less']
})
export class StreamAdminCreateComponent implements OnInit {
  streamAdd: Stream = new Stream(0, '', '', []);

  constructor(private activatedRouter: ActivatedRoute,
              private streamService: StreamService,
              private streamEventService: StreamEventService,
              private route: Router,
  private popupsService: PopupsService) {
  }

  ngOnInit() {
  }


  addStream(createStreamBtn: MatButton) {

    if (this.streamAdd.name === '') {
    } else {
      createStreamBtn.disabled = true;
      this.activatedRouter.parent.params.subscribe(params => {
        const add = new Stream(this.streamAdd.id, this.streamAdd.location, this.streamAdd.name, []);
        this.streamService.add(+params['id'], add).subscribe(receivedStream => {
          const eventId = params['id'];
          this.streamEventService.addEvent(receivedStream);
          this.streamAdd = new Stream(0, '', '', []);
          this.route.navigate(['events', eventId, 'streams', receivedStream.id, 'topics'] , { replaceUrl: true});
        });
      });
    }
  }
}
