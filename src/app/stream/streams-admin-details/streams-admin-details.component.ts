import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatDialogRef} from '@angular/material';
import {ConfirmDialogWindowComponent, EventNavigationSubjectService, Stream, StreamCreateService, StreamEventService, StreamEventType, StreamService} from 'qq-core-lib';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-streams-admin-details',
  templateUrl: './streams-admin-details.component.html',
  styleUrls: ['./streams-admin-details.component.less']
})
export class StreamsAdminDetailsComponent implements OnInit, OnDestroy {
  streams: Stream[] = [];
  eventId = 0;
  eventSubjectReference: any;
  createNewStream = false;
  confirmDialogWindows: MatDialogRef<ConfirmDialogWindowComponent>;


  private subscription: Subscription;

  constructor(private router: Router,
              private streamService: StreamService,
              private activadedRout: ActivatedRoute,
              private streamEventService: StreamEventService,
              private streamCreateService: StreamCreateService,
              private dialog: MatDialog,
              private eventSaveNotifier: EventNavigationSubjectService) {
  }

  ngOnInit() {
    this.activadedRout.params.subscribe(params => {
      this.streamService.getStreamsByEvent(params['id']).subscribe(receivedStreams => {
        this.streams = receivedStreams;
        this.eventId = params['id'];
        if (this.streams.length === 0) {
          this.router.navigate(['events', params['id'], 'streams'], {replaceUrl: true});
        } else {
          this.router.navigate(['events', params['id'], 'streams', this.streams[0].id, 'topics'], {replaceUrl: true});
        }
      });
    });
    this.eventSubjectReference = this.streamEventService.getEvent().subscribe(recivedStreamEvent => {
      if (recivedStreamEvent.eventType === StreamEventType.add) {
        this.streams.push(recivedStreamEvent.stream);
      }
      if (recivedStreamEvent.eventType === StreamEventType.delete) {
        this.onDelete(recivedStreamEvent.stream);
      }
    });

    this.streamCreateService.subject.subscribe(action => {
      if (action['action'] === false) {
        this.createNewStream = false;
      }
      if (action['action'] === true) {
        this.streams.push(action['stream']);
        this.router.navigate(['events', this.eventId, 'streams', action['stream'].id, 'topics'], {replaceUrl: true});
        this.createNewStream = false;
      }
    });

    this.subscription = this.eventSaveNotifier.subscribe((routeEnabler) => {
        routeEnabler.redirectEnabler.next(
          {
            allowRedirect: true,
            eventId: this.eventId.toString()
          })
    });
  }

  onDelete(stream: Stream) {
    this.streams = this.streams.filter(str => str.id !== stream.id);
    this.activadedRout.params.subscribe(params => {
      this.streamService.getStreamsByEvent(params['id']).subscribe(receivedStreams => {
        this.streams = receivedStreams;
        if (this.streams.length === 0) {
          this.router.navigate(['events', params['id'], 'streams'], {replaceUrl: true});
        } else {
          this.router.navigate(['events', params['id'], 'streams', this.streams[0].id, 'topics'], {replaceUrl: true});
        }
      });
    });
  }

  deleteStream(stream: Stream) {
    this.confirmDialogWindows = this.dialog.open(ConfirmDialogWindowComponent,
      {
        data: {
          text: 'Are you sure want to remove ' + stream.name + '?',
          topic: stream, positiveAnswer: 'Yes', negativeAnswer: 'No'
        }
      });
    this.confirmDialogWindows.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.streamService.delete(stream).subscribe(() => {
          this.streamEventService.deleteEvent(stream);
        });
      }
    });
  }


  ngOnDestroy(): void {
    this.eventSubjectReference.unsubscribe();
    this.subscription.unsubscribe();
  }


  onNewStreamClick() {
    this.createNewStream = true;
  }

}
