///<reference path="../../../../node_modules/@angular/common/http/src/interceptor.d.ts"/>
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HeaderAdminComponent} from './app-admin-header/app-header.admin.component';
import {MaterialModule} from '../../material/material.module';
import {RouterModule} from '@angular/router';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {ProgressInterceptor, ProgressModule} from 'qq-core-lib';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    ProgressModule
  ],
  declarations: [
    HeaderAdminComponent
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: ProgressInterceptor, multi: true}
  ],
  exports: [
    HeaderAdminComponent
  ]
})
export class AdminCoreModule { }
