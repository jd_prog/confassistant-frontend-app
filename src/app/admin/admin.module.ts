import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../material/material.module';
import {StreamModule} from '../stream/stream.module';
import {AdminViewComponent} from './admin-view/admin-view.component';
import {AdminCoreModule} from './admin-core/admin-core.module';
import {AdminRoutingModule} from './admin-routing.component';
import {EventModule} from '../event/event.module';
import {SecurityModule} from '../security/security.module';


@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    EventModule,
    StreamModule,
    AdminCoreModule,
    AdminRoutingModule,
    SecurityModule
  ],
  declarations: [
    AdminViewComponent
  ],

  exports: [
    AdminViewComponent
  ]
})
export class AdminModule {
}
