import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HeaderAdminComponent} from './admin-core/app-admin-header/app-header.admin.component';
import {EventAdminComponent} from '../event/events-admin-view/event.admin.component';
import {AdminAuthGuard} from '../security/security-core/guards/admin-auth.guard';
import {TopicsAdminViewComponent} from '../topic/topics-admin-view/topics-admin-view.component';
import {EventsAdminCreateComponent} from '../event/events-admin-create/events-admin-create.component';
import {EventsAdminDetailsComponent} from '../event/events-admin-details/events-admin-details.component';
import {StreamsAdminDetailsComponent} from '../stream/streams-admin-details/streams-admin-details.component';
import {EventStatisticComponent} from '../event/event-statistic/event-statistic.component';

import {AdminEventPermeationGuard} from 'qq-core-lib';
import {AdminPingServerGuard} from './admin-ping-server/admin-ping-server-guard.service';

export const routes: Routes = [

  {
    path: '', component: HeaderAdminComponent, canActivate: [AdminAuthGuard],
    data: {
      expectedRole: 'ROLE_ADMIN'
    },
    children:
      [{path: 'events', component: EventAdminComponent, canActivate: [AdminPingServerGuard]},
        {
          path: 'events/new', component: EventsAdminCreateComponent, children: [
            {path: '', component: EventsAdminDetailsComponent}
          ], canActivate: [AdminPingServerGuard]
        },
        {path: 'events/:eventId', redirectTo: 'events/:eventId/details'},
        {
          path: 'events/:id',
          component: EventsAdminCreateComponent,
          canActivate: [AdminEventPermeationGuard, AdminPingServerGuard],
          children: [
            {path: 'details', component: EventsAdminDetailsComponent},
            {
              path: 'streams', component: StreamsAdminDetailsComponent, children: [
                {path: ':streamId/topics', component: TopicsAdminViewComponent},
                {path: ':streamId/topics/:topicId', component: TopicsAdminViewComponent}
              ]
            },
            {path: 'statistic', component: EventStatisticComponent}
          ]
        }
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}

