import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {EventModule} from './event/event.module';
import {StreamModule} from './stream/stream.module';
import {TopicModule} from './topic/topic.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SecurityModule} from './security/security.module';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {AdminModule} from './admin/admin.module';
import {FormsModule} from '@angular/forms';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {environment} from '../environments/environment';
import {AngularFireModule} from 'angularfire2';
import {QQRestConfig} from 'qq-core-lib';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';
import {ErrorModule} from "./error/error.module";
import {MatTableModule} from '@angular/material/table';
import {registerLocaleData} from '@angular/common';
import localeEu from '@angular/common/locales/eu';
import localeUk from '@angular/common/locales/uk';
import localeEuExtra from '@angular/common/locales/extra/eu';
import localeUkExtra from '@angular/common/locales/extra/uk';
import {FlexLayoutModule} from "@angular/flex-layout";



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'confassistant-app'}),
    BrowserAnimationsModule,
    EventModule,
    StreamModule,
    TopicModule,
    SecurityModule,
    AppRoutingModule,
    HttpClientModule,
    MatTableModule,
    AdminModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    DropDownsModule,
    ErrorModule,
    FlexLayoutModule
  ],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor() {
    QQRestConfig.baseUrl(environment.serverUrl);
  }
}
