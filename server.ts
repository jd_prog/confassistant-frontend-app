import 'zone.js/dist/zone-node';
import 'reflect-metadata';
import {enableProdMode} from '@angular/core';
// Express Engine
import {ngExpressEngine} from '@nguniversal/express-engine';
// Import module map for lazy loading
import {provideModuleMap} from '@nguniversal/module-map-ngfactory-loader';

import * as express from 'express';
import {join} from 'path';

// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

// Express server
const app = express();

const PORT = process.env.PORT || 4000;
const DIST_FOLDER = join(process.cwd(), 'dist');

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const {AppServerModuleNgFactory, LAZY_MODULE_MAP} = require('./server/main');

// Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
app.engine('html', ngExpressEngine({
  bootstrap: AppServerModuleNgFactory,
  providers: [
    provideModuleMap(LAZY_MODULE_MAP)
  ]
}));

app.set('view engine', 'html');
app.set('views', join(DIST_FOLDER, 'browser'));


const router = express.Router();
//serve's actual resources, e.x index.html
router.get(/(en|uk)\/*/, express.static(join(DIST_FOLDER, 'browser'), {
  maxAge: '1y'
}));

router.get('/:locale?/*?/', (req, res) =>{
  if (['en', 'uk'].includes(req.params.locale)) {
    res.render(`${req.params.locale}`, { req });
  } else {
    const language = getBrowserOrDefaultLanguage(req);
    const restOfUrl = req.params[0];
    const urlWithCorrectLocale = restOfUrl ? `${req.baseUrl}/${language}/${req.params[0]}` : `${req.baseUrl}/${language}`;
    res.redirect(301, urlWithCorrectLocale);
  }
})

app.use('/admin', router)

// Start up the Node server
app.listen(PORT, () => {
  console.log(`Node Express server listening on http://localhost:${PORT}`);
});

function getBrowserOrDefaultLanguage(req) {
  const browserLanguage = req.headers['accept-language'].substring(0, 2);
  return browserLanguage ? browserLanguage : 'en';
}
