import subprocess
import os
import requests

get_confassistant_admin ='https://'+os.environ['NEXUS_DOCKER_PATH']+'/v2/qq/'+os.environ['CONFASSISTANT_CONTAINER_NAME']+'-'+os.environ['BITBUCKET_BRANCH']+'/manifests/latest'
get_qq = 'https://'+os.environ['NEXUS_DOCKER_PATH']+'/v2/qq/'+os.environ['QQ_CONTAINER_NAME']+'-'+os.environ['BITBUCKET_BRANCH']+'/manifests/latest'
get_confassistant = 'https://'+os.environ['NEXUS_DOCKER_PATH']+'/v2/qq/'+os.environ['NEXUS_BACK_END_NAMING']+'-'+os.environ['BITBUCKET_BRANCH']+'/manifests/latest'
# resp = requests.get('https://vitech.com.ua:5001/v2/qq/confassistant-back-end/manifests/latest', auth=('vladyslav.raikovskyi','VitechPass2018'))
confassistant_admin_request = requests.get(get_confassistant_admin, auth=('vladyslav.raikovskyi','VitechPass2018'))
qq_request = requests.get(get_qq, auth=('vladyslav.raikovskyi','VitechPass2018'))
confassistant_request = requests.get(get_confassistant, auth=('vladyslav.raikovskyi','VitechPass2018'))


try:
  ## build back-end test enviroment
  subprocess.check_output("docker login -u$DOCKER_HUB_LOGIN -p$DOCKER_HUB_PASSWORD $SERVER", shell=True)
  ##################################### db   #######################################
  is_test_db_container_running = subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER ps -aq -f name=$DB_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True)
  if is_test_db_container_running != '' :
    subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER stop $DB_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True)
    subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER rm $DB_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True)
    subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_DB", shell=True)
    branch = os.environ['BITBUCKET_BRANCH']
    port = int(filter(str.isdigit, branch))
    port = '3'+`port`+'4'
    os.environ["RUNNING_PORT"] = port
    subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER run -itd --network=$NETWORK --link $DB_CONTAINER_NAME-$BITBUCKET_BRANCH:db-$BITBUCKET_BRANCH --name=$DB_CONTAINER_NAME-$BITBUCKET_BRANCH -d -p $RUNNING_PORT:5432 $NEXUS_REPOSITORY_DB", shell=True)
  else :
    subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_DB", shell=True)
    branch = os.environ['BITBUCKET_BRANCH']
    port = int(filter(str.isdigit, branch))
    port = '3'+`port`+'4'
    os.environ["RUNNING_PORT"] = port
    subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER run -itd --network=$NETWORK --name=$DB_CONTAINER_NAME-$BITBUCKET_BRANCH -d -p $RUNNING_PORT:5432 $NEXUS_REPOSITORY_DB", shell=True)
  ##################################### java #######################################
  if confassistant_request.status_code == requests.codes.ok :
    if subprocess.check_output("docker  --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER ps -aq -f name=$BACK_END_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True) != '' :
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER stop $BACK_END_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True)
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER rm $BACK_END_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True)
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_BACK_END-$BITBUCKET_BRANCH", shell=True)
      branch = os.environ['BITBUCKET_BRANCH']
      port = int(filter(str.isdigit, branch))
      port = '3'+`port`+'1'
      os.environ["RUNNING_PORT"] = port
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER run -itd --network=$NETWORK --link $DB_CONTAINER_NAME-$BITBUCKET_BRANCH:db-$BITBUCKET_BRANCH --name=$BACK_END_CONTAINER_NAME-$BITBUCKET_BRANCH -d -p $RUNNING_PORT:8081 $NEXUS_REPOSITORY_BACK_END-$BITBUCKET_BRANCH", shell=True)
    elif subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_BACK_END-$BITBUCKET_BRANCH", shell=True) != '' :
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_BACK_END-$BITBUCKET_BRANCH", shell=True)
      branch = os.environ['BITBUCKET_BRANCH']
      port = int(filter(str.isdigit, branch))
      port = '3'+`port`+'1'
      os.environ["RUNNING_PORT"] = port
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER run -itd --network=$NETWORK --link $DB_CONTAINER_NAME-$BITBUCKET_BRANCH:db-$BITBUCKET_BRANCH --name=$BACK_END_CONTAINER_NAME-$BITBUCKET_BRANCH -d -p $RUNNING_PORT:8081 $NEXUS_REPOSITORY_BACK_END-$BITBUCKET_BRANCH", shell=True)
  else:
    if subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER ps -aq -f name=$BACK_END_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True) != '' :
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER stop $BACK_END_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True)
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER rm $BACK_END_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True)
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_BACK_END", shell=True)
      branch = os.environ['BITBUCKET_BRANCH']
      port = int(filter(str.isdigit, branch))
      port = '3'+`port`+'1'
      os.environ["RUNNING_PORT"] = port
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER run -itd --network=$NETWORK --link $DB_CONTAINER_NAME-$BITBUCKET_BRANCH:db-$BITBUCKET_BRANCH --name=$BACK_END_CONTAINER_NAME-$BITBUCKET_BRANCH -d -p $RUNNING_PORT:8081 $NEXUS_REPOSITORY_BACK_END", shell=True)
    else:
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_BACK_END", shell=True)
      branch = os.environ['BITBUCKET_BRANCH']
      port = int(filter(str.isdigit, branch))
      port = '3'+`port`+'1'
      os.environ["RUNNING_PORT"] = port
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER run -itd --network=$NETWORK --link $DB_CONTAINER_NAME-$BITBUCKET_BRANCH:db-$BITBUCKET_BRANCH --name=$BACK_END_CONTAINER_NAME-$BITBUCKET_BRANCH -d -p $RUNNING_PORT:8081 $NEXUS_REPOSITORY_BACK_END", shell=True)
  ##################################### client app #######################################
  if qq_request.status_code == requests.codes.ok :
    if subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER ps -aq -f name=$QQ_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True) != '' :
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER stop $QQ_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True)
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER rm $QQ_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True)
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_QQ-$BITBUCKET_BRANCH", shell=True)
      branch = os.environ['BITBUCKET_BRANCH']
      port = int(filter(str.isdigit, branch))
      port = '3'+`port`+'2'
      os.environ["RUNNING_PORT"] = port
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER run --name=$QQ_CONTAINER_NAME-$BITBUCKET_BRANCH -d -p $RUNNING_PORT:4001 $NEXUS_REPOSITORY_QQ-$BITBUCKET_BRANCH", shell=True)
    elif subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_QQ-$BITBUCKET_BRANCH", shell=True) != '' :
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_QQ-$BITBUCKET_BRANCH", shell=True)
      branch = os.environ['BITBUCKET_BRANCH']
      port = int(filter(str.isdigit, branch))
      port = '3'+`port`+'2'
      os.environ["RUNNING_PORT"] = port
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER run --name=$QQ_CONTAINER_NAME-$BITBUCKET_BRANCH -d -p $RUNNING_PORT:4001 $NEXUS_REPOSITORY_QQ-$BITBUCKET_BRANCH", shell=True)
  else:
    if subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER ps -aq -f name=$QQ_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True) != '' :
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER stop $QQ_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True)
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER rm $QQ_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True)
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_QQ", shell=True)
      branch = os.environ['BITBUCKET_BRANCH']
      port = int(filter(str.isdigit, branch))
      port = '3'+`port`+'2'
      os.environ["RUNNING_PORT"] = port
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER run --name=$QQ_CONTAINER_NAME-$BITBUCKET_BRANCH -d -p $RUNNING_PORT:4001 $NEXUS_REPOSITORY_QQ", shell=True)
    else:
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_QQ", shell=True)
      branch = os.environ['BITBUCKET_BRANCH']
      port = int(filter(str.isdigit, branch))
      port = '3'+`port`+'2'
      os.environ["RUNNING_PORT"] = port
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER run --name=$QQ_CONTAINER_NAME-$BITBUCKET_BRANCH -d -p $RUNNING_PORT:4001 $NEXUS_REPOSITORY_QQ", shell=True)
  ##################################### confassistant app #######################################
  if confassistant_admin_request.status_code == requests.codes.ok :
    if subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER ps -aq -f name=$CONFASSISTANT_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True) != '' :
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER stop $CONFASSISTANT_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True)
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER rm $CONFASSISTANT_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True)
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_CONFASSISTANT-$BITBUCKET_BRANCH", shell=True)
      branch = os.environ['BITBUCKET_BRANCH']
      port = int(filter(str.isdigit, branch))
      port = '3'+`port`+'3'
      os.environ["RUNNING_PORT"] = port
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER run --name=$CONFASSISTANT_CONTAINER_NAME-$BITBUCKET_BRANCH -d -p $RUNNING_PORT:4000 $NEXUS_REPOSITORY_CONFASSISTANT-$BITBUCKET_BRANCH", shell=True)
    elif subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_CONFASSISTANT-$BITBUCKET_BRANCH", shell=True) != '' :
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_CONFASSISTANT-$BITBUCKET_BRANCH", shell=True)
      branch = os.environ['BITBUCKET_BRANCH']
      port = int(filter(str.isdigit, branch))
      port = '3'+`port`+'3'
      os.environ["RUNNING_PORT"] = port
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER run --name=$CONFASSISTANT_CONTAINER_NAME-$BITBUCKET_BRANCH -d -p $RUNNING_PORT:4000 $NEXUS_REPOSITORY_CONFASSISTANT-$BITBUCKET_BRANCH", shell=True)
  else:
    if subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER ps -aq -f name=$CONFASSISTANT_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True) != '' :
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER stop $CONFASSISTANT_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True)
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER rm $CONFASSISTANT_CONTAINER_NAME-$BITBUCKET_BRANCH", shell=True)
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_CONFASSISTANT", shell=True)
      branch = os.environ['BITBUCKET_BRANCH']
      port = int(filter(str.isdigit, branch))
      port = '3'+`port`+'3'
      os.environ["RUNNING_PORT"] = port
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER run --name=$CONFASSISTANT_CONTAINER_NAME-$BITBUCKET_BRANCH -d -p $RUNNING_PORT:4000 $NEXUS_REPOSITORY_CONFASSISTANT", shell=True)
    else:
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER pull $NEXUS_REPOSITORY_CONFASSISTANT", shell=True)
      branch = os.environ['BITBUCKET_BRANCH']
      port = int(filter(str.isdigit, branch))
      port = '3'+`port`+'3'
      os.environ["RUNNING_PORT"] = port
      subprocess.check_output("docker --tlsverify --tlscacert=$tlscacert --tlscert=$tlscert --tlskey=$tlskey -H=$REMOTE_SERVER run --name=$CONFASSISTANT_CONTAINER_NAME-$BITBUCKET_BRANCH -d -p $RUNNING_PORT:4000 $NEXUS_REPOSITORY_CONFASSISTANT", shell=True)

except subprocess.CalledProcessError as e:
  print e.output
