import io
import os

with open("src/environments/environment.test.ts", 'r') as file:
  # read a list of lines into data
  data = file.readlines()

branch = os.environ['BITBUCKET_BRANCH']
port = int(filter(str.isdigit, branch))
port = '3'+`port`+'1'
# now change the 2nd line, note that you have to add a newline
data[2] = "  serverUrl: 'http://qq.vitech.com.ua:"+port+"/' ,\n"
print "  serverUrl: 'http://qq.vitech.com.ua:"+port+"/' ,\n"


# and write everything back
with open("src/environments/environment.test.ts", 'w') as file:
  file.writelines( data )
