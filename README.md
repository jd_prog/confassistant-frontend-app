# ConfassistantApp
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

This project consists of client and admin apps.
Client app allows you to view events, topics, view/rate/post questions.
Admin app provides functionality to create events, streams, topics, scpeakers.

To start using client app follow the link: https://confassistant-frontend-app.herokuapp.com/
You should use Facebook account to login.

To enter lo admin part of the app follow this link: https://confassistant-frontend-app.herokuapp.com/admin/login
User these credentials to login as admin:
Username: admin
Password: admin

## Technology on project
- Angular 6

## Project scructure
- src
	- Project separated on modules by principle 'one feature - one module'. Each module contains core module and list of components. Code module contains domain models, services, pipes, directives etc. Components should be named by principle 'moduleName'-'partOfAppWhereItUsed'-'featureName'. For example, event-client-details related with event.module, used in client part of the app and implements functionality to display event details
- environments
	- There're prod and default (dev) environtent. Prod environment configured to make api calls to prod server hosted on heroku (https://confassistant-backend-app.herokuapp.com). Default environment configured to work with localhost server (http://localhost:800)

## CI
App is hosted on Heroku.
Bitbucket pipelines user to manage deployment processes.
Every time after developer pushed some changes on his brang, ng build --prod will performed, to check if there're no errors and app is building successfully in prod mode.
When developer branch is merged with master, build script runs, too. If build script finishes successfully, app is pushed to heroku repository.

## Development server
To run app locally, run `npm i` to download dependencies and `ng serve` to start a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


### Code scaffolding
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
